package sonalakeExcercise;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


@TestMethodOrder(OrderAnnotation.class)
public class OpenCalc {	
	
	private static Calculator calc;

	@BeforeAll
	public static void prepareTests() throws InterruptedException
	{
		WebDriver wb = new FirefoxDriver();
		calc = new Calculator(wb);
		calc.init();
	}
	
	@AfterAll
	public static void quit()
	{
		calc.exit();
	}


	@Test
	@Order(1)
	void calculation() throws InterruptedException, HeadlessException, UnsupportedFlavorException, IOException  {
		String result = calc.performCalculation();
		assertEquals("34990", result);
	}
	
	@Test
	@Order(2)
	void cosine() throws InterruptedException, HeadlessException, UnsupportedFlavorException, IOException  {
		String result = calc.performCosPi();
		assertEquals("-1", result);
	}
	
	@Test
	@Order(3)
	void squareRoot() throws InterruptedException, HeadlessException, UnsupportedFlavorException, IOException  {
		String result = calc.performSqrt();
		assertEquals("9", result);
	}
	
	@Test
	@Order(4)
	void historyCheck() throws InterruptedException, HeadlessException, UnsupportedFlavorException, IOException  {
		String[] expected = {"35*999+(100/4)", "cos(pi)", "sqrt(81)"};
		String[] result = calc.historyCheck();
		Arrays.sort(expected);
		Arrays.sort(result);
		assertArrayEquals(expected, result);
	}

}
