package sonalakeExcercise;

import java.awt.HeadlessException;
import java.awt.List;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Calculator {
	WebDriver driver;
	Actions act;

	
	By agreeButton = By.xpath("//*[text()[contains(.,'AGREE')]]");
	By cosineButton = By.id("BtnCos");
	By leftParanButton = By.id("BtnParanL");
	By rightParanButton = By.id("BtnParanR");
	By multiplyButton = By.id("BtnMult");
	By plusButton = By.id("BtnPlus");
	By divideButton = By.id("BtnDiv");
	By sqrtButton = By.id("BtnSqrt");
	By radButton = By.xpath("//*[@title='Radian']");
	By historyDropdown = By.id("hist");
	By calculateButton = By.id("BtnCalc");
	By clearButton = By.id("BtnClear");
	By piButton = By.id("BtnPi");
	By input = By.xpath("//*[@id='input']");
	By firstHistElem = By.xpath("/html/body/div[5]/div[3]/ul/li[1]/p[2]");
	By secondHistElem = By.xpath("/html/body/div[5]/div[3]/ul/li[2]/p[2]");
	By thirdHistElem = By.xpath("/html/body/div[5]/div[3]/ul/li[3]/p[2]");
	 
	public Calculator(WebDriver driver) {
		this.driver = driver;
		this.act = new Actions(driver);
		driver.get("http://web2.0calc.com/");
	}
	
	public void init() { 
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(3)).until(ExpectedConditions.elementToBeClickable(agreeButton));
		foo.click();
	}
		
	public void exit() {
		driver.quit();
	}
	
	public String getResultText() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {
		StringSelection stringSelection = new StringSelection("");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		act.click(driver.findElement(input)).perform();
		act.keyDown(Keys.CONTROL).perform();
		act.sendKeys("a").perform();
		act.keyUp(Keys.CONTROL).perform();	
		act.keyDown(Keys.CONTROL).perform();
		act.sendKeys("c").perform();
		act.keyUp(Keys.CONTROL).perform();
		String data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
	    return data;
	}
	
	public String performCalculation() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {
		driver.findElement(input).sendKeys("35");
		driver.findElement(multiplyButton).click();
		driver.findElement(input).sendKeys("999");
		driver.findElement(plusButton).click();
		driver.findElement(leftParanButton).click();
		driver.findElement(input).sendKeys("100");
		driver.findElement(divideButton).click();
		driver.findElement(input).sendKeys("4");
		driver.findElement(rightParanButton).click();
		driver.findElement(calculateButton).click();
		return getResultText();
	}
	
	public String performCosPi() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {
		driver.findElement(clearButton).click();
		driver.findElement(radButton).click();
		driver.findElement(piButton).click();
		driver.findElement(cosineButton).click();
		driver.findElement(calculateButton).click();
		return getResultText();
	}
	
	public String performSqrt() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {
		driver.findElement(clearButton).click();
		driver.findElement(input).sendKeys("81");
		driver.findElement(sqrtButton).click();
		driver.findElement(calculateButton).click();
		return getResultText();
	}
	
	public String[] historyCheck() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {
		ArrayList<String> arr = new ArrayList<String>();
		driver.findElement(clearButton).click();
		driver.findElement(historyDropdown).click();
		driver.findElement(firstHistElem).click();
		arr.add(getResultText());
		driver.findElement(secondHistElem).click();
		arr.add(getResultText());
		driver.findElement(thirdHistElem).click();
		arr.add(getResultText());
		String[] result = new String[arr.size()];
		result = arr.toArray(result);
		return result;
	}

}
