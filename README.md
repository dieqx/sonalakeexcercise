## The project

This is Maven project (maven-compiler-plugin version 3.8.0, configuration an source 1.8), together with depencencies installed. 
If those don't work by themselves, here they are:

  <dependencies>
  <dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-api</artifactId>
    <version>5.7.0</version>
    <scope>test</scope>
</dependency>
   <dependency>
     <groupId>org.seleniumhq.selenium</groupId>
       <artifactId>selenium-java</artifactId>
       <version>4.0.0-alpha-7</version>
   </dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-api</artifactId>
    <version>1.7.30</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.30</version>
</dependency>
  </dependencies>

---

## Running the tests

- JUnit execution on OpenCalc.java file.
- geckodriver (in my OS I didn't have to specify its location), so Firefox web browser is necessary

---

## Comments

** What could've been done:
- better optimisation and perhaps refactoring in Calculator.java class
- cleanup of unused imports
- splitting Calculator.java into objects/methods files